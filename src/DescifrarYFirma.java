import javax.crypto.Cipher;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Scanner;

public class DescifrarYFirma {
     String algoritmo;
     PublicKey publicKey;
     PrivateKey privateKey;
     byte[] mensajeCifrado;
     byte[] firma;

    public DescifrarYFirma(String algoritmo, PublicKey publicKey, PrivateKey privateKey, byte[] mensajeCifrado, byte[] firma) {
        this.algoritmo = algoritmo;
        this.publicKey = publicKey;
        this.privateKey = privateKey;
        this.mensajeCifrado = mensajeCifrado;
        this.firma = firma;
    }
    // Método para descifrar el mensaje y verificar la firma
    public String descifrarYVerificarFirma() {
        try {
            // Descifrar el mensaje con la clave pública
            Cipher cipher = Cipher.getInstance(algoritmo);
            cipher.init(Cipher.DECRYPT_MODE, publicKey);
            byte[] mensajeDescifrado = cipher.doFinal(mensajeCifrado);
            String mensaje = new String(mensajeDescifrado, StandardCharsets.UTF_8);
            // Verificar la firma
            Signature signature = Signature.getInstance("SHA256withRSA");
            signature.initVerify(publicKey);// Inicializar el objeto Signature con la clave pública
            signature.update(mensaje.getBytes(StandardCharsets.UTF_8));// Actualizar con el mensaje descifrado
            boolean verificado = signature.verify(firma); // Verificar la firma
            // Devolver el mensaje si la firma es válida, de lo contrario, indicar que no coincide
            if (verificado) {
                return mensaje;
            } else {
                return "La firma no coincide.";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Error al descifrar y verificar la firma.";
        }
    }
}



