import javax.crypto.Cipher;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.util.Scanner;

public class CifradoYFirma {

     String algoritmo;
     PublicKey publicKey;
     PrivateKey privateKey;
     String mensajeNormal;
     byte[] mensajeCifrado;
     byte[] firma;

    public CifradoYFirma(String algoritmo, PublicKey publicKey, PrivateKey privateKey, String mensajeNormal) {
        this.algoritmo = algoritmo;
        this.publicKey = publicKey;
        this.privateKey = privateKey;
        this.mensajeNormal = mensajeNormal;
    }
        // Método para cifrar el mensaje y firmarlo digitalmente
    public void cifrarYFirmar() {
        // Obtener la ruta del directorio actual del proyecto
        Path ruta = Paths.get(System.getProperty("user.dir"));
        System.out.println(ruta);
        // Rutas de los archivos donde se almacenarán el mensaje cifrado y la firma
        Path ruta_fichero = ruta.resolve("mensajeCifrado");
        Path ruta_fichero2 = ruta.resolve("firmaMensaje");
        System.out.println(ruta_fichero);
        //creamos el objetos de la ruta y verificamos si existe
        File ficherom = new File(ruta_fichero.toString());
        File ficherof = new File(ruta_fichero2.toString());
        //si NO existe
        if(!ficherom.exists()){
            try {
                //creando el ficherom
                ficherom.createNewFile();
                System.out.println(ficherom);
                ficherof.createNewFile();
                System.out.println(ficherof);
            } catch (IOException e) {
                System.out.println("No se pudo crear el ficherom");
            }
        }else {
            try {
                // Cifrar el mensaje con la clave privada
                Cipher cipher = Cipher.getInstance(algoritmo);
                cipher.init(Cipher.ENCRYPT_MODE, privateKey);
                mensajeCifrado = cipher.doFinal(mensajeNormal.getBytes(StandardCharsets.UTF_8));
                // Firmar digitalmente el mensaje
                Signature signature = Signature.getInstance("SHA256withRSA");
                signature.initSign(privateKey);
                signature.update(mensajeNormal.getBytes(StandardCharsets.UTF_8));
                firma = signature.sign();
                // Guardar el mensaje cifrado en un archivo
                FileOutputStream mensajeCifradoFile = new FileOutputStream("mensajeCifrado");
                mensajeCifradoFile.write(mensajeCifrado);
                mensajeCifradoFile.close();
                System.out.println("Mensaje cifrado guardado en mensajeCifrado");
                // Guardar la firma en un archivo
                FileOutputStream firmaMensajeFile = new FileOutputStream("firmaMensaje");
                firmaMensajeFile.write(firma);
                firmaMensajeFile.close();
                System.out.println("Firma del mensaje guardada en firmaMensaje");

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
