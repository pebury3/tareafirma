import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Scanner;

public class GeneradorClaves {

     String algoritmo;
     PublicKey publicKey;
     PrivateKey privateKey;
    public GeneradorClaves(String algoritmo) {

        this.algoritmo = algoritmo;
    }
    // Método para generar claves y escribirlas en archivos
    public void generar() {
        Scanner sc = new Scanner(System.in);
        // Obtener la ruta del directorio actual del proyecto
        Path ruta = Paths.get(System.getProperty("user.dir"));
        System.out.println(ruta);
        // Rutas de los archivos donde se almacenarán las claves
        Path ruta_fichero = ruta.resolve("publicKey");
        Path ruta_fichero2 = ruta.resolve("privateKey");
        System.out.println(ruta_fichero);
        //creamos el objetos  de la ruta y verificamos si existe
        File fichero = new File(ruta_fichero.toString());
        File fichero2 = new File(ruta_fichero2.toString());
        //si NO existe
        if (!fichero.exists()) {
            try {
                //creando el fichero
                fichero.createNewFile();
                System.out.println(fichero);
                fichero2.createNewFile();
                System.out.println(fichero2);
            } catch (IOException e) {
                System.out.println("No se pudo crear el fichero");
            }
        } else {
            //si existe escribimos dentro
            try {
                // Generar el par de claves
                KeyPairGenerator keygen = KeyPairGenerator.getInstance(algoritmo);
                KeyPair keyPair = keygen.genKeyPair();
                 publicKey = keyPair.getPublic();
                 privateKey = keyPair.getPrivate();
                // Inicializar el objeto Cipher para cifrar la clave privada
                Cipher cipher = Cipher.getInstance(algoritmo);
                // Solicitar al usuario ingresar la clave a escribir
                System.out.println("dime la clave a escribir");
                String clave1 = sc.nextLine();
                // Cifrar la clave privada y escribirla en el archivo
                cipher.init(Cipher.ENCRYPT_MODE, privateKey);
                byte[] ClaveCifrada = cipher.doFinal(clave1.getBytes());
                System.out.println(new String(ClaveCifrada));
                System.out.println("se ha escrito la clave privada");
                System.out.println();
                FileOutputStream fw = new FileOutputStream(fichero);
                fw.write(privateKey.getEncoded());
                // Escribir la clave pública en el segundo archivo
                FileOutputStream fw2 = new FileOutputStream(fichero2);
                fw2.write(publicKey.getEncoded());

                fw.close();
                fw2.close();
                System.out.println("Cerrando fichero");

            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            } catch (NoSuchPaddingException e) {
                throw new RuntimeException(e);
            } catch (IllegalBlockSizeException e) {
                throw new RuntimeException(e);
            } catch (BadPaddingException e) {
                throw new RuntimeException(e);
            } catch (InvalidKeyException e) {
                throw new RuntimeException(e);
            }
        }
    }
    // Método para obtener la clave pública generada
    public PublicKey PublicKey() {
        return publicKey;
    }
    // Método para obtener la clave privada generada
    public PrivateKey PrivateKey() {
        return privateKey;
    }
}
