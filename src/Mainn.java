import java.io.FileInputStream;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Scanner;

public class Mainn {
    public static void main(String[] args) {
        try {
            // Generar las claves
            GeneradorClaves generador = new GeneradorClaves("RSA");
            generador.generar();
            PublicKey publicKey = generador.PublicKey();
            PrivateKey privateKey = generador.PrivateKey();

            // Cifrar y firmar un mensaje
            Scanner sc = new Scanner(System.in);
            System.out.print("Introduce el mensaje a cifrar y firmar: ");
            System.out.println(" ");
            String mensaje = sc.nextLine();
            sc.close();
            CifradoYFirma cifradoFirma = new CifradoYFirma("RSA", publicKey, privateKey, mensaje);
            cifradoFirma.cifrarYFirmar();
            // Leer el mensaje cifrado y la firma
            FileInputStream mensajeCifradoStream = new FileInputStream("mensajeCifrado");
            byte[] mensajeCifrado = mensajeCifradoStream.readAllBytes();
            mensajeCifradoStream.close();
            FileInputStream firmaStream = new FileInputStream("firmaMensaje");
            byte[] firma = firmaStream.readAllBytes();
            firmaStream.close();
            // Descifrar el mensaje y verificar la firma
            DescifrarYFirma descifrarFirma = new DescifrarYFirma("RSA", publicKey, privateKey, mensajeCifrado, firma);
            String mensajeDescifrado = descifrarFirma.descifrarYVerificarFirma();
            System.out.println("Mensaje descifrado y verificado: " + mensajeDescifrado);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
